package fr.fam.webexporter;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import fr.fam.webexporter.config.WebConfig;

/**
*
*/
@WebListener
public class WebServletContextListener implements ServletContextListener {

    /** */
    private static final Logger LOGGER = Logger.getLogger(WebServletContextListener.class);

    @Override
    public final void contextInitialized(final ServletContextEvent sce) {

        WebConfig config = new WebConfig();
        WebCollector collector = new WebCollector(config);

        ServletContext sc = sce.getServletContext();
        sc.setAttribute("collector", collector);

        LOGGER.info("ServletContextListener started");
    }

    @Override
    public final void contextDestroyed(final ServletContextEvent sce) {

        ServletContext sc = sce.getServletContext();
        WebCollector collector = (WebCollector) sc.getAttribute("collector");
        collector.close();
        sc.removeAttribute("collector");

        LOGGER.info("ServletContextListener destroyed");
    }
}
