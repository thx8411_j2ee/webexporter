package fr.fam.webexporter;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Request;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import fr.fam.webexporter.config.Source;

/**
* Scrap probe from a defined source.
*/
public class WebScraper implements Callable<Void> {

    /** Logger. */
    private static final Logger LOGGER = Logger.getLogger(WebScraper.class);

    /** Probe source. */
    private Source source;

    /** Query header. */
    private Request httpRequest;

    /**
    * Constructor.
    *
    * @param psource source
    */
    public WebScraper(final Source psource) {
        source = psource;

        buildHttpRequest();
    }

    /**
    * Call method, for task execution.
    *
    * @return Void void wrapper
    */
    public final Void call() {

        LOGGER.debug("Scraping source : " + source.toString());

        // check the probe
        checkProbe();

        // nothing to return (Void)
        return null;
    }

    /**
    * Query the probe.
    *
    * @return The javamelody http response or null if the request failed
    */
    private final void checkProbe() {
        try {
            // request
            HttpResponse httpResponse = httpRequest.execute().returnResponse();

            // check http response status
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if (responseCode == HttpStatus.SC_OK) {
                LOGGER.debug("HTTP-Response OK ");

                // check pattern in page content
                String body = EntityUtils.toString(httpResponse.getEntity());
                LOGGER.debug("Body : " + body);
                if(body.contains(source.getPattern())) {
                    source.setValue(100.0);
                    LOGGER.debug("Probe OK");
                } else {
                    source.setValue(0.0);
                    LOGGER.debug("Probe KO");
                }

            } else {
                source.setValue(0.0);
                LOGGER.warn("Probe unreachable, using 0");
                LOGGER.warn("HTTP-Response code was " + responseCode + " for " + source.getUrl());
            }
        // we catch all exceptions to keep the app running
        } catch (Exception e) {
            LOGGER.warn("Exception while downloading: " + source.getUrl(), e);
        }
    }

    /**
    * Encode credentials in base64.
    *
    * @param login login
    * @param password password
    * @return base64 encoded credentials
    */
    private String buildBasicAuthHeaderValue(final String login, final String password) {
        String credentials = login + ":" + password;
        return Base64.encodeBase64String(credentials.getBytes());
    }

    /**
    * Build the http request.
    */
    private void buildHttpRequest() {
        Header authHeader;

        // build the http request
        Request request = Request.Get(source.getUrl())
            .connectTimeout(source.getTimeout())
            .socketTimeout(source.getTimeout());

        // create basic authentification header if needed
        if (source.getLogin() != null && source.getPassword() != null) {
            authHeader = new BasicHeader("Authorization", "Basic"
                + buildBasicAuthHeaderValue(source.getLogin(), source.getPassword()));
            request = request.addHeader(authHeader);
        }

        httpRequest = request;
    }

}
