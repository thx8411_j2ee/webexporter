package fr.fam.webexporter.config;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
*
*/
public class Application {
    /** */
    private String name;

    /** */
    private List<Source> sources = new ArrayList<Source>();

    /**
    *
    * @return name
    */
    public final String getName() {
        return name;
    }

    /**
    *
    * @param pname application name
    */
    public final void setName(final String pname) {
        name = pname;
    }

    /**
    *
    * @return sources
    */
    public final List<Source> getSources() {
        return sources;
    }

    /**
    *
    * @param psources sources
    */
    public final void setSources(final List<Source> psources) {
        sources = psources;
    }

    /**
    *
    * @return string
    */
    @Override
    public final String toString() {
        StringBuilder s = new StringBuilder();

        s.append("{name=" + name + ", ");

        // browsing sources
        s.append("Sources[");
        for (Source a : sources) {
            s.append(a.toString());
            s.append(", ");
        }

        s.append("]");

        s.append("}");

        return s.toString();
    }

}
