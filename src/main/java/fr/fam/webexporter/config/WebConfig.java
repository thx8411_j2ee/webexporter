package fr.fam.webexporter.config;

import java.io.InputStream;
import java.util.Scanner;

import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import org.apache.log4j.Logger;

/**
*
*/
public class WebConfig {

    /** */
    private static final Logger LOGGER = Logger.getLogger(WebConfig.class);

    /** */
    private static final String SETTINGS_FILENAME = "webexporter.yml";

    /** */
    private static Applications applications;

    /**
    *
    * @throws IllegalStateException IllegalStateException
    */
    public WebConfig() throws IllegalStateException {
        LOGGER.debug("Reading configuration...");

        try {
            // Get applications file and open it
            InputStream settingsInputStream = Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream(SETTINGS_FILENAME);

            // Parse settings file
            LOGGER.debug("Parsing yaml file...");

            // load applications
            Yaml yamlApplications = new Yaml(new Constructor(Applications.class));
            applications = (Applications) yamlApplications.load(yamlRead(settingsInputStream));
            LOGGER.info("Applications loaded : " + applications.getApplications().size());

            // check config integrity
            checkApplications();

            LOGGER.debug("Applications : " + applications.toString());

        } catch (Exception e) {
                LOGGER.error("Can't read settings file : " + SETTINGS_FILENAME);
                LOGGER.error("Exception : ", e);
                throw new IllegalStateException("Can't read settings file");
        }
    }

    /**
    *
    * @return applications
    */

    public final Applications getApplications() {
        return applications;
    }

    /**
    * Read the yaml documents into string.
    *
    * @param in inputStream
    * @return string
    */
    private String yamlRead(final InputStream in) {
        Scanner s = new Scanner(in).useDelimiter("\\A");

        String buffer;
        if (s.hasNext()) {
            buffer = s.next();
        } else {
            buffer = "";
        }

        return ("applications:\n" + buffer);
    }

    /**
    *
    * @throws IllegalStateException IllegalStateException
    */
    private void checkApplications() throws IllegalStateException {
        LOGGER.info("Checking config...");

        // for each application
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources()) {

                // check url
                if (source.getUrl() == null) {
                    LOGGER.error("Missing url for application : " + application.getName());
                    throw new IllegalStateException("Missing url for application : " + application.getName());
                }

            }
        }
        LOGGER.info("Done.");

    }

}
