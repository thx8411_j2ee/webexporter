package fr.fam.webexporter.config;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
*
*/
public class Source {
    /** */
    static final int DEFAULT_TIMEOUT = 5000;

    /** */
    static final double DEFAULT_VALUE = 0.0;

    /** */
    private String url;

    /** */
    private String pattern;

    /** */
    private int timeout = DEFAULT_TIMEOUT;

    /** */
    private String login;

    /** */
    private String password;

    /** */
    private String env;

    /** */
    private double value = DEFAULT_VALUE;

    /**
    *
    * @return url
    */
    public final String getUrl() {
        return url;
    }

    /**
    *
    * @param purl application url
    */
    public final void setUrl(final String purl) {
        url = purl;
    }

    /**
    *
    * @return pattern
    */
    public final String getPattern() {
        return pattern;
    }

    /**
    *
    * @param ppattern pattern to check
    */
    public final void setPattern(final String ppattern) {
        pattern = ppattern;
    }

    /**
    *
    * @return timeout
    */
    public final int getTimeout() {
        return timeout;
    }

    /**
    *
    * @param ptimeout timeout
    */
    public final void setTimeout(final int ptimeout) {
        timeout = ptimeout;
    }

    /**
    *
    * @return login
    */
    public final String getLogin() {
        return login;
    }

    /**
    *
    * @param plogin application login
    */
    public final void setLogin(final String plogin) {
        login = plogin;
    }

    /**
    *
    * @return password
    */
    public final String getPassword() {
        return password;
    }

    /**
    *
    * @param ppassword application password
    */
    public final void setPassword(final String ppassword) {
        password = ppassword;
    }

    /**
    *
    * @return env
    */
    public final String getEnv() {
        return env;
    }

    /**
    *
    * @param penv source env
    */
    public final void setEnv(String penv) {
        env = penv;
    }

   /**
    *
    * @return value
    */
    public final double getValue() {
        return value;
    }

    /**
    *
    * @param pvalue souce value
    */
    public final void setValue(double pvalue) {
        value = pvalue;
    }

    /**
    *
    * @return string
    */
    @Override
    public final String toString() {
        StringBuilder s = new StringBuilder();

        s.append("url=" + url + ", ");
        s.append("timeout=" + timeout + ", ");
        s.append("login=" + login + ", ");
        s.append("password=" + "*******" + ", ");
        s.append("env=" + env);

        return s.toString();
    }
}
