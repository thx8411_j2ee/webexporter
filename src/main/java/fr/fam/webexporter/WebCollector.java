package fr.fam.webexporter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import fr.fam.webexporter.config.WebConfig;
import fr.fam.webexporter.config.Applications;
import fr.fam.webexporter.config.Application;
import fr.fam.webexporter.config.Source;
import io.prometheus.client.Collector;
import io.prometheus.client.GaugeMetricFamily;

/**
*
*/
public class WebCollector extends Collector {

    /** full scraping timeout (in seconds). */
    static final long COLLECTOR_TIMEOUT = 10L;

    /** */
    private static final Logger LOGGER = Logger.getLogger(WebCollector.class);

    /** */
    private Set<WebScraper> scrapers = new HashSet<WebScraper>();

    /** */
    private WebConfig config;

    /** */
    private Applications applications;

    /** threads pool. */
    private ExecutorService scrapPool;

    /**
    *
    * @param pconfig configuration
    */
    public WebCollector(final WebConfig pconfig) {
        super();
        config = pconfig;
        applications = config.getApplications();

        // populate scrapers for each source
        // for each app
        LOGGER.debug("Creating scrapers...");
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources()) {

                // create and add scraper
                scrapers.add(new WebScraper(source));
            }
        }
        LOGGER.debug("Done.");

        // create the thread pool
        scrapPool = Executors.newFixedThreadPool(scrapers.size());
    }

    /**
    *
    */
    public final void close() {
        scrapPool.shutdownNow();
    }

    /**
    *
    * @throws IllegalStateException
    */
    @Override
    public final List<MetricFamilySamples> collect() throws IllegalStateException {
        LOGGER.debug("Building samples...");

        List<MetricFamilySamples> mfs = new ArrayList<MetricFamilySamples>();

        // scrap sources
        // scraping is multithreaded
        // we scrap by sources using sources.getMetrics(), so it's thread safe
        // we use invokeAll to wait for all tasks to finish
        // So, scrapers need to be callable objects
        try {
            scrapPool.invokeAll(scrapers, COLLECTOR_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Timeout on source scraping thread pool");
        }

        // label keys list
        List<String> labelKeys = new ArrayList<String>();
        labelKeys.add("application");
        labelKeys.add("source");
        labelKeys.add("env");

        // create gauge
        GaugeMetricFamily gauge = new GaugeMetricFamily("webexporter_probeAvailability", "Help for webexporter_probeAvailability", labelKeys);

        // for each application
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources()) {

                LOGGER.debug("New source : " + source.getUrl());

                // label values list
                List<String> labelValues = new ArrayList<String>();
                labelValues.add(application.getName());
                labelValues.add(source.getUrl());
                labelValues.add(source.getEnv());

                // populate gauge
                gauge.addMetric(labelValues, source.getValue());

	    }

        }

        mfs.add(gauge);

        return mfs;
    }

}
